package com.rdndeveloper.entities;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import com.rdndeveloper.main.Game;
import com.rdndeveloper.world.Camera;
import com.rdndeveloper.world.World;

public class Player extends Entity {

	public boolean right, up, left, down;
	public int right_direction = 0, left_direction = 1;
	public int direction = right_direction;
	public double speed = 1.4;
	private int frames = 0, maxFrames = 5, index = 0, maxIndex = 3;
	private boolean moved = false;
	private BufferedImage[] rightPlayer;
	private BufferedImage[] leftPlayer;
	private BufferedImage playerDamage;
	public double life = 100, maxLife = 100;
	public int ammo = 0;
	public boolean isDamage = false;
	private int damageFrames = 0;
	private boolean temArma = false;
	public boolean atirando = false;
	public boolean mouseAtirando = false;
	public int mx, my;
	public boolean jump = false, isJumping = false, jumpUp = false, jumpDown = false;
	public int z = 0;
	public int jumpFrames = 50, jumpCurrent = 0;
	
	public Player(int x, int y, int width, int height, BufferedImage sprite)
	{
		super(x, y, width, height, sprite);
		rightPlayer = new BufferedImage[4];
		leftPlayer = new BufferedImage[4];
		playerDamage = Game.sprite.getSprite(0, 16, 16, 16);
		
		renderImages(rightPlayer, 0);
		renderImages(leftPlayer, 16);
	}
	
	public void renderImages(BufferedImage[] array, int y)
	{
		for (int i = 0; i < array.length; i++) {
			array[i] = Game.sprite.getSprite(32 + (i * 16), y, 16, 16);
		}
	}
	
	public void tick()
	{
		moved = false;
		
		if (jump) {
			if (isJumping == false) {
				jump = false;
				isJumping = true;
				jumpUp = true;
			}
		}
		
		if (isJumping) {
			if (jumpUp) {
				jumpCurrent+=2;
			} else if (jumpDown) {
				jumpCurrent-=2;
				if (jumpCurrent <= 0) {
					isJumping = false;
					jumpDown = false;
					jumpUp = false;
				}
			}
			z = jumpCurrent;
			if (jumpCurrent >= jumpFrames) {
				jumpUp = false;
				jumpDown = true;
			}
		}
		
		if (right && World.isFree((int)(x+speed), this.getY(), this.getZ())) {
			moved = true;
			direction = right_direction;
			x += speed;
		} else if (left && World.isFree((int)(x-speed), this.getY(), this.getZ())) {
			moved = true;
			direction = left_direction;
			x -= speed;
		}
		
		if (up && World.isFree(this.getX(), (int)(y-speed), this.getZ())) {
			moved = true;
			y -= speed;
		} else if (down && World.isFree(this.getX(), (int)(y+speed), this.getZ())) {
			moved = true;
			y += speed;
		}
		
		if (moved) {
			frames++;
			if (frames == maxFrames) {
				frames = 0;
				index++;
				if (index > maxIndex) {
					index = 0;
				}
			}
		}
		
		this.checkCollisionLifepack();
		this.checkCollisionAmmo();
		this.checkCollisionWeapon();
		
		if (isDamage) {
			this.damageFrames++;
			if (this.damageFrames == 8) {
				this.damageFrames = 0;
				isDamage = false;
			}
		}
		
		if (atirando) {
			// criar bala e atirar
			atirando = false;
			if (temArma && ammo > 0) {
				ammo--;
				int dx = (direction == right_direction) ? 1 : -1;
				int px = (direction == right_direction) ? 18 : -8;
				int py = 6;
				
				BulletShoot bullet = new BulletShoot(this.getX() + px, this.getY() + py, 3, 3, null, dx, 0);
				Game.bullets.add(bullet);
			}
		}
		
		if (mouseAtirando) {
			// criar bala e atirar
			mouseAtirando = false;
			if (temArma && ammo > 0) {
				ammo--;
				int px = 0, py = 8;
				double angle = 0;
				if (direction == right_direction) {
					px = 18;
					angle = Math.atan2(my - (this.getY() + py - Camera.y), mx - (this.getX() + px - Camera.x));
				} else {
					px = -8;
					angle = Math.atan2(my - (this.getY() + py - Camera.y), mx - (this.getX() + px - Camera.x));
				}
				
				double dx = Math.cos(angle);
				double dy = Math.asin(angle);
				
				BulletShoot bullet = new BulletShoot(this.getX() + px, this.getY() + py, 3, 3, null, dx, dy);
				Game.bullets.add(bullet);
			}
		}
		
		if (life <= 0) {
			// Game over
			life = 0;
			Game.gameState = "GAME_OVER";
			return;
		}
		
		this.updateCamera();
	}
	
	public void updateCamera()
	{
		Camera.x = Camera.clamp(this.getX() - (Game.WIDTH / 2), 0, World.WIDTH*16 - Game.WIDTH);
		Camera.y = Camera.clamp(this.getY() - (Game.HEIGHT / 2), 0, World.HEIGHT*16 - Game.HEIGHT);
	}
	
	public void checkCollisionLifepack()
	{
		for (int i = 0; i < Game.entities.size(); i++) {
			Entity atual = Game.entities.get(i);
			if (atual instanceof LifePack) {
				if (Entity.isColidding(this, atual)) {
					life += 10;
					
					if (life >= 100) 
						life = 100;
					
					Game.entities.remove(i);
				}
			}
		}
	}
	
	public void checkCollisionAmmo()
	{
		for (int i = 0; i < Game.entities.size(); i++) {
			Entity atual = Game.entities.get(i);
			if (atual instanceof Bullet) {
				if (Entity.isColidding(this, atual)) {
					ammo+=100;
					Game.entities.remove(i);
				}
			}
		}
	}
	
	public void checkCollisionWeapon()
	{
		for (int i = 0; i < Game.entities.size(); i++) {
			Entity atual = Game.entities.get(i);
			if (atual instanceof Weapon) {
				if (Entity.isColidding(this, atual)) {
					temArma = true;
					Game.entities.remove(i);
				}
			}
		}
	}
	
	public void render(Graphics g)
	{
		if (!isDamage) {
			if (direction == right_direction) {
				g.drawImage(rightPlayer[index], this.getX() - Camera.x, this.getY() - Camera.y - z, null);
				if (temArma) {
					// desenha arma para direita
					g.drawImage(Entity.WEAPON_RIGHT, this.getX() + 7 - Camera.x, this.getY() - Camera.y - z, null);
				}
			} else if (direction == left_direction) {
				g.drawImage(leftPlayer[index], this.getX() - Camera.x, this.getY() - Camera.y - z, null);
				if (temArma) {
					// desenha arma para esquerda
					g.drawImage(Entity.WEAPON_LEFT, this.getX() - 7 - Camera.x, this.getY() - Camera.y - z, null);
				}
			}
		} else {
			g.drawImage(playerDamage, this.getX() - Camera.x, this.getY() - Camera.y - z, null);
		}
		
		if (isJumping) {
			g.setColor(Color.black);
			g.fillOval(this.getX()-Camera.x + 3, this.getY()-Camera.y + 16, 8, 8);
			
		}
	}
	
}
