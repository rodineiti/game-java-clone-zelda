package com.rdndeveloper.entities;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

import com.rdndeveloper.main.Game;
import com.rdndeveloper.main.Sound;
import com.rdndeveloper.world.Camera;
import com.rdndeveloper.world.World;

public class Enemy extends Entity {

	private double speed = 0.4;
	private int maskx = 8, masky = 8, maskw = 10, maskh = 10;
	private int frames = 0, maxFrames = 20, index = 0, maxIndex = 1;
	private BufferedImage[] sprites;
	private int life = 2;
	private boolean isDamaged = false;
	private int damageFrames = 10, damageCurrent = 0;
	
	public Enemy(int x, int y, int width, int height, BufferedImage sprite) {
		super(x, y, width, height, null);
		
		sprites = new BufferedImage[2];
		sprites[0] = Game.sprite.getSprite(112, 16, World.TILE_SIZE, World.TILE_SIZE);
		sprites[1] = Game.sprite.getSprite(112+16, 16, World.TILE_SIZE, World.TILE_SIZE);
	}
	
	public void tick()
	{
		if (isColindingWithPlayer() == false) {
			if ((int)x < Game.player.getX() && World.isFree((int)(x+speed), this.getY(), this.getY()) 
					&& !isColiding((int)(x+speed), this.getY())) {
				x += speed;
			} else if ((int)x > Game.player.getX() && World.isFree((int)(x-speed), this.getY(), this.getZ()) 
					&& !isColiding((int)(x-speed), this.getY())) {
				x -= speed;
			}
			
			if ((int)y < Game.player.getY() && World.isFree(this.getX(), (int)(y+speed), this.getZ()) 
					&& !isColiding(this.getX(), (int)(y+speed))) {
				y += speed;
			} else if ((int)y > Game.player.getY() && World.isFree(this.getX(), (int)(y-speed), this.getZ()) 
					&& !isColiding(this.getX(), (int)(y-speed))) {
				y -= speed;
			}
		} else {
			// estamos perto do player - estamos colidindo
			// o que podemos fazer
			if (Game.rand.nextInt(100) < 10) {
				Sound.hurtEffect.play();
				Game.player.life -= Game.rand.nextInt(3);
				Game.player.isDamage = true;
				//System.out.println("Vida " + Game.player.life);
			}
		}
		
		frames++;
		if (frames == maxFrames) {
			frames = 0;
			index++;
			if (index > maxIndex) {
				index = 0;
			}
		}
		
		collidingBullet();
		
		if (life <= 0) {
			destroySelf();
			return;
		}
		
		if (isDamaged) {
			this.damageCurrent++;
			if (this.damageFrames == this.damageCurrent) {
				this.damageCurrent = 0;
				this.isDamaged = false;
			}
		}
	}
	
	public void destroySelf() {
		Game.enemies.remove(this);
		Game.entities.remove(this);
	}
	
	public void collidingBullet()
	{
		for (int i = 0; i < Game.bullets.size(); i++) {
			Entity e = Game.bullets.get(i);
			if (e instanceof BulletShoot) {
				if (Entity.isColidding(this, e)) {
					isDamaged = true;
					life--;
					Game.bullets.remove(i);
					return;
				}
			}
		}
	}
	
	public boolean isColindingWithPlayer()
	{
		Rectangle enemyCurrent = new Rectangle(this.getX() + maskx, this.getY() + masky, maskw, maskh);
		Rectangle player = new Rectangle(Game.player.getX(), Game.player.getY(), 16,16);
		return enemyCurrent.intersects(player);
	}

	public boolean isColiding(int xnext, int ynext)
	{
		Rectangle enemyCurrent = new Rectangle(xnext + maskx, ynext + masky, maskw, maskh);
		for (int i = 0; i < Game.enemies.size(); i++) {
			Enemy e = Game.enemies.get(i);
			if (e == this)
				continue;
			Rectangle targetCurrent = new Rectangle(e.getX() + maskx, e.getY() + masky, maskw, maskh);
			
			if (enemyCurrent.intersects(targetCurrent)) {
				return true;
			}
		}
		return false;
	}
	
	public void render(Graphics g)
	{
		//super.render(g);
		//g.setColor(Color.blue);
		//g.fillRect(this.getX() + maskx - Camera.x, this.getY() + masky - Camera.y, maskw, maskh);
		
		if (!isDamaged) {
			g.drawImage(sprites[index], this.getX() - Camera.x, this.getY() - Camera.y, null);
		} else {
			g.drawImage(Entity.ENEMY_FEEDBACK, this.getX() - Camera.x, this.getY() - Camera.y, null);
		}
	}

}
