package com.rdndeveloper.world;

public class Camera {

	public static int x;
	public static int y;
	
	public static int clamp(int Current, int Min, int Max)
	{
		if (Current < Min) {
			Current = Min;
		}
		
		if (Current > Max) {
			Current = Max;
		}
		
		return Current;
	}
}
