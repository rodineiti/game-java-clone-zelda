package com.rdndeveloper.world;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.imageio.ImageIO;

import com.rdndeveloper.entities.*;
import com.rdndeveloper.main.Game;

public class World {
	
	private static Tile[] tiles;
	public static int WIDTH, HEIGHT;
	public static final int TILE_SIZE = 16;
	
	public World(String path)
	{
		try {
			BufferedImage map = ImageIO.read(getClass().getResource(path));
			int[] pixels = new int[map.getWidth() * map.getHeight()]; // quantos pixels tem na imagem
			map.getRGB(0, 0, map.getWidth(), map.getHeight(), pixels, 0, map.getWidth());
			tiles = new Tile[map.getWidth() * map.getHeight()];
			WIDTH = map.getWidth();
			HEIGHT = map.getHeight();
			
			for (int w = 0; w < map.getWidth(); w++) {
				for (int y = 0; y < map.getHeight(); y++) {
					int current = pixels[w+(y*map.getWidth())];
					tiles[w + (y*WIDTH)] = new FloorTile(w * 16, y * 16, Tile.TILE_FLOOR);
					if (current == 0xFF000000) {
						// Floor
						tiles[w + (y*WIDTH)] = new FloorTile(w * 16, y * 16, Tile.TILE_FLOOR);
					} else if (current == 0xFFFFFFFF) {
						// Wall
						tiles[w + (y*WIDTH)] = new WallTile(w * 16, y * 16, Tile.TILE_WALL);
					} else if (current == 0xFF0026FF) {
						// Player
						//tiles[w + (y*WIDTH)] = new FloorTile(w * 16, y * 16, Tile.TILE_FLOOR);
						Game.player.setX(w*16);
						Game.player.setY(y*16);
					} else if (current == 0xFFFF0000) {
						// Enemy
						Enemy enem = new Enemy(w*16, y*16, 16, 16, Entity.ENEMY_EN);
						Game.entities.add(enem);
						Game.enemies.add(enem);
					} else if (current == 0xFFFF6A00) {
						// Weapon
						Game.entities.add(new Weapon(w*16, y*16, 16, 16, Entity.WEAPON_EN));
					} else if (current == 0xFFFF7F7F) {
						// Life pack
						LifePack pack = new LifePack(w*16, y*16, 16, 16, Entity.LIFEPACK_EN);
						Game.entities.add(pack);
					} else if (current == 0xFFFFD800) {
						// Bullet
						Game.entities.add(new Bullet(w*16, y*16, 16, 16, Entity.BULLET_EN));
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static boolean isFree(int xnext, int ynext, int zplayer)
	{
		int x1 = xnext / TILE_SIZE;
		int y1 = ynext / TILE_SIZE;
		
		int x2 = (xnext+TILE_SIZE-1) / TILE_SIZE;
		int y2 = ynext / TILE_SIZE;
		
		int x3 = xnext / TILE_SIZE;
		int y3 = (ynext+TILE_SIZE-1) / TILE_SIZE;
		
		int x4 = (xnext+TILE_SIZE-1) / TILE_SIZE;
		int y4 = (ynext+TILE_SIZE-1) / TILE_SIZE;
		
		if (!(
				(tiles[x1 + (y1 * World.WIDTH)] instanceof WallTile) || 
				(tiles[x2 + (y2 * World.WIDTH)] instanceof WallTile) || 
				(tiles[x3 + (y3 * World.WIDTH)] instanceof WallTile) || 
				(tiles[x4 + (y4 * World.WIDTH)] instanceof WallTile)
				)) {
			return true;
		}
		
		if (zplayer > 0) {
			return true;
		}
		return false;
	}
	
	public static void restartGame(String level)
	{
		Game.load(level);
	}
	
	public void render(Graphics g)
	{
		int xStart = Camera.x / 16;
		int yStart = Camera.y / 16;
		int xFinal = (xStart + (Game.WIDTH / 16));
		int yFinal = (yStart + (Game.HEIGHT / 16));
		
		for (int w = xStart; w <= xFinal; w++) {
			for (int y = yStart; y <= yFinal; y++) {
				if (w < 0 || y < 0 || w >= WIDTH || y >= HEIGHT)
					continue;
				Tile tile = tiles[w + (y*WIDTH)];
				tile.render(g);
			}
		}
	}
}
