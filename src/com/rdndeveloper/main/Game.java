package com.rdndeveloper.main;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;

import com.rdndeveloper.entities.BulletShoot;
import com.rdndeveloper.entities.Enemy;
import com.rdndeveloper.entities.Entity;
import com.rdndeveloper.entities.Player;
import com.rdndeveloper.graficos.Spritesheet;
import com.rdndeveloper.graficos.UI;
import com.rdndeveloper.world.World;

public class Game extends Canvas implements Runnable, KeyListener, MouseListener {
	
	private static final long serialVersionUID = 1L;
	public static JFrame frame;
	private Thread thread;
	private boolean isRunning = true;
	public static final int WIDTH = 240;
	public static final int HEIGHT = 160;
	public static final int SCALE = 3;
	private BufferedImage image;
	public static List<Entity> entities;
	public static List<Enemy> enemies;
	public static List<BulletShoot> bullets;
	public static Spritesheet sprite;
	public static Player player;
	public static World world;
	public static Random rand;
	public UI ui;
	private int CUR_LEVEL = 1, MAX_LEVEl = 2;
	public static String gameState = "MENU";
	private boolean gameOver = false;
	private int framesGameOver = 0;
	private boolean restartGame = false;
	public static Menu menu;
	public boolean saveGame = false;
	
	public Game()
	{
		Sound.musicBackground.loop();
		rand = new Random();
		addKeyListener(this);
		addMouseListener(this);
		setPreferredSize(new Dimension(WIDTH * SCALE, HEIGHT * SCALE));
		initFrame();
		ui = new UI();
		image = new BufferedImage(WIDTH,HEIGHT, BufferedImage.TYPE_INT_ARGB);
		load("");
	}
	
	public static void load(String level)
	{
		entities = new ArrayList<Entity>();
		enemies = new ArrayList<Enemy>();
		bullets = new ArrayList<BulletShoot>();
		sprite = new Spritesheet("/spritesheet.png");
		player = new Player(0, 0, 16, 16, sprite.getSprite(32, 0, 16, 16));
		entities.add(player);
		if (level == "") {
			level = "level1.png";
		}
		world = new World("/" + level);
		
		menu = new Menu();
	}
	
	public void initFrame()
	{
		frame = new JFrame("Game Demo");
		frame.add(this);
		frame.setResizable(false);
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	
	public synchronized void start()
	{
		thread = new Thread(this);
		isRunning = true;
		thread.start();
	}
	
	public synchronized void stop()
	{
		isRunning = false;
		try {
			thread.join();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main (String args[])
	{
		Game game = new Game();
		game.start();
	}
	
	public void tick()
	{
		if (gameState == "NORMAL") {
			
			if (this.saveGame) {
				this.saveGame = false;
				String[] opt1 = {"level","life","ammo"};
				int[] opt2 = {this.CUR_LEVEL, (int)player.life, player.ammo};
				Menu.saveGame(opt1, opt2, 10);
				System.out.println("Jogo salvo com sucesso");
			}
			
			this.restartGame = false;
			for (int i = 0; i < entities.size(); i++) {
				Entity e = entities.get(i);
				e.tick();
			}
			
			for (int i = 0; i < bullets.size(); i++) {
				bullets.get(i).tick();
			}
			
			if (enemies.size() == 0) {
				CUR_LEVEL++;
				if (CUR_LEVEL > MAX_LEVEl) {
					CUR_LEVEL = 1;
				}
				String newWorld = "level" + CUR_LEVEL + ".png";
				World.restartGame(newWorld);
			}
		} else if (gameState == "GAME_OVER") {
			this.framesGameOver++;
			if (this.framesGameOver == 30) {
				this.framesGameOver = 0;
				if (this.gameOver) {
					this.gameOver = false;
				} else {
					this.gameOver = true;
				}
			}
			
			if (restartGame) {
				this.restartGame = false;
				this.gameState = "NORMAL";
				CUR_LEVEL = 1;
				String newWorld = "level" + CUR_LEVEL + ".png";
				World.restartGame(newWorld);
			}
		} else if (gameState == "MENU") {
			menu.tick();
		}
	}
	
	public void render()
	{
		BufferStrategy bs = this.getBufferStrategy();
		if (bs == null) {
			this.createBufferStrategy(3);
			return;
		}
		Graphics g = image.getGraphics();
		g.setColor(new Color(0,0,0));
		g.fillRect(0, 0, WIDTH, HEIGHT);
		
		// renderizar image no jogo
		//Graphics2D g2 = (Graphics2D) g;
		world.render(g);
		for (int i = 0; i < entities.size(); i++) {
			Entity e = entities.get(i);
			e.render(g);
		}
		for (int i = 0; i < bullets.size(); i++) {
			bullets.get(i).render(g);
		}
		ui.render(g);
		
		g.dispose();
		g = bs.getDrawGraphics();
		g.drawImage(image, 0, 0, WIDTH*SCALE, HEIGHT*SCALE, null);
		g.setFont(new Font("arial", Font.BOLD, 20));
		g.setColor(Color.white);
		g.drawString("Munição: " + Game.player.ammo, 570, 20);
		if (gameState == "GAME_OVER") {
			Graphics2D g2 = (Graphics2D)g;
			g2.setColor(new Color(0,0,0,100));
			g2.fillRect(0, 0, WIDTH*SCALE, HEIGHT*SCALE);
			
			g.setFont(new Font("arial", Font.BOLD, 36));
			g.setColor(Color.white);
			g.drawString("Game Over", (WIDTH*SCALE) / 2 - 100, (HEIGHT*SCALE) / 2 - 20);
			g.setFont(new Font("arial", Font.BOLD, 30));
			if (gameOver)
				g.drawString("> Pressione Enter para reiniciar", (WIDTH*SCALE) / 2 - 240, (HEIGHT*SCALE) / 2 + 40);
		} else if (gameState == "MENU") {
			menu.render(g);
		}
		bs.show();
	}
	
	public void run()
	{
		long lastTime = System.nanoTime(); // tempo atual do computador em nanosegundos
		double amountOfTicks = 60.0; // 60 frames por segundo
		double ns = 1000000000 / amountOfTicks;
		double delta = 0;
		int frames = 0;
		double timer = System.currentTimeMillis();
		
		requestFocus();
		
		while(isRunning) {
			long now = System.nanoTime();
			delta += (now - lastTime) / ns;
			lastTime = now;
			if (delta >= 1) {
				tick();
				render();
				frames++;
				delta--;
			}
			
			if (System.currentTimeMillis() - timer >= 1000) {
				//System.out.println("FPS: " + frames);
				frames = 0;
				timer += 1000;
			}
		}
		
		stop();
	}

	@Override
	public void keyPressed(KeyEvent event) {
		if (event.getKeyCode() == KeyEvent.VK_Z) {
			player.jump = true;
		}
		
		if (event.getKeyCode() == KeyEvent.VK_RIGHT || event.getKeyCode() == KeyEvent.VK_D) {
			// move para direita
			player.right = true;
		} else if (event.getKeyCode() == KeyEvent.VK_LEFT || event.getKeyCode() == KeyEvent.VK_A) {
			// move para esquerda
			player.left = true;
		}
		
		if (event.getKeyCode() == KeyEvent.VK_UP || event.getKeyCode() == KeyEvent.VK_W) {
			// move para cima
			player.up = true;
			if (gameState == "MENU") {
				menu.up = player.up;
			}
		} else if (event.getKeyCode() == KeyEvent.VK_DOWN || event.getKeyCode() == KeyEvent.VK_S) {
			// move para baixo
			player.down = true;
			if (gameState == "MENU") {
				menu.down = player.down;
			}
		}
		
		if (event.getKeyCode() == KeyEvent.VK_X) {
			player.atirando = true;
		}
		
		if (event.getKeyCode() == KeyEvent.VK_ENTER) {
			this.restartGame = true;
			if (gameState == "MENU") {
				menu.enter = true;
			}
		}
		
		if (event.getKeyCode() == KeyEvent.VK_ESCAPE) {
			gameState = "MENU";
			Menu.pause = true;
		}
		
		if (event.getKeyCode() == KeyEvent.VK_SPACE) {
			if (gameState == "NORMAL")
				this.saveGame = true;
		}
	}

	@Override
	public void keyReleased(KeyEvent event) {
		if (event.getKeyCode() == KeyEvent.VK_RIGHT || event.getKeyCode() == KeyEvent.VK_D) {
			// move para direita
			player.right = false;
		} else if (event.getKeyCode() == KeyEvent.VK_LEFT || event.getKeyCode() == KeyEvent.VK_A) {
			// move para esquerda
			player.left = false;
		}
		
		if (event.getKeyCode() == KeyEvent.VK_UP || event.getKeyCode() == KeyEvent.VK_W) {
			// move para cima
			player.up = false;
		} else if (event.getKeyCode() == KeyEvent.VK_DOWN || event.getKeyCode() == KeyEvent.VK_S) {
			// move para baixo
			player.down = false;
		}		
	}

	@Override
	public void keyTyped(KeyEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		player.mouseAtirando = true;
		player.mx = (arg0.getX() / SCALE);
		player.my = (arg0.getY() / SCALE);
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}

