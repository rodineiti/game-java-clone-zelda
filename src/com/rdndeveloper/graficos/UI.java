package com.rdndeveloper.graficos;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import com.rdndeveloper.main.Game;

public class UI {
	
	public void render(Graphics g)
	{
		int width = 70;
		g.setColor(Color.red);
		g.fillRect(8,4, width, 8);
		g.setColor(Color.green);
		g.fillRect(8,4, (int)((Game.player.life/Game.player.maxLife)*width), 8);
		g.setColor(Color.white);
		g.setFont(new Font("arial", Font.BOLD, 8));
		g.drawString((int)Game.player.life + "/" + (int)Game.player.maxLife,23,11);
	}
}
